/** Bài 1
 * Input: số ngày làm
 *
 * Step:
 * 1. tạo ra 2 biến chứa tiền lương 1 ngày và số ngày làm
 * 2. áp dụng lương = lương 1 ngày * số ngày
 *
 *
 * Output: lương
 */

var luongNgay = 100000;
var soNgayLam = 24;
var luong;
luong = luongNgay * soNgayLam;
console.log("Lương: ", luong);

/** Bài 2
 * Input: giá trị của 5 số
 *
 * Step:
 * 1. tạo ra  biến 5 số
 * 2. áp dụng tính trung bình
 *
 *
 * Output: số trung bình
 */

var num1, num2, num3, num4, num5, numAvg;
num1 = 7;
num2 = 5;
num3 = 1;
num4 = 9;
num5 = 0;

numAvg = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("Số trung bình cộng: ", numAvg);

/** Bài 3
 * Input: số tiền USD
 *
 * Step:
 * 1. tạo ra 2 biến số
 * 2. áp dụng tỉ giá quy đổi
 *
 *
 * Output: số tiền VND
 */

var soTienUsd, tyGia, soTienVnd;
tyGia = 23500;
soTienUsd = 2;
soTienVnd = soTienUsd * tyGia;
console.log("Số tiền sau quy đổi: ", soTienVnd + " VND");

/** Bài 4
 * Input: giá trị chiều dài, chiều rộng
 *
 * Step:
 * 1. tạo ra 2 biến số chiều dài, chiều rộng
 * 2. áp dụng công thức tính diện tích và chu vi
 *
 *
 * Output: chu vi, diện tích
 */

var chieuDai, chieuRong, chuVi, dienTich;
chieuDai = 7;
chieuRong = 5;
chuVi = (chieuDai + chieuRong) * 2;
console.log("Chu vi: ", chuVi);
dienTich = chieuDai * chieuRong;
console.log("Diện Tích: ", dienTich);

/** Bài 5
 * Input: số có 2 chữ số
 *
 * Step:
 * 1. tạo ra 2 biến số của hàng đơn vị, hàng chục
 * 2. tính các hàng đơn vị, chục
 *
 *
 * Output: tổng 2 số
 */

var num = 75;
var hangDonVi, hangChuc, tong;
hangDonVi = num % 10;
hangChuc = Math.floor((num % 100) / 10);
tong = hangDonVi + hangChuc;
console.log("Tổng 2 ký số: ", tong);
